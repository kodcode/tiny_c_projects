/* The function of this program is to sort an array of structures
 * in alphabetic order. */

#include <stdio.h>

struct phonebook
{
	char firstName[16];
	char lastName[16];
	int  phoneNumber;
};

int
main(void)
{
	struct phoneBook friends[4] = {{"Donald", "Duck",     12345}, 
                                       {"Mickey", "Mouse",    54321},
				       {"Mario",  "Brothers", 43233},
				       {"Calvin", "",         88822},
				       {"Hobbes", "",         23145},
				       {"Bart",   "Simpson",  12333}};

	return 0;
}

