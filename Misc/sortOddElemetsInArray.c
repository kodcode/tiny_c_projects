/*
A program that sorts only the odd elements of an array
in ascending order.
*/

#include <stdio.h>

int main(void)
{
    int i;
    int n = 7;
    int arr[7] = { 4, 13, 5, 8, 3, 2, 6 };
    void sort_array(size_t n, int arr[n]);


    printf("The elements of the array before sorting are:");
    for (i = 0; i < n; ++i)
	printf(" %d", arr[i]);
    printf("\n");

    sort_array(n, arr);

    printf("The *odd* elements arrenged in ascending order are:");
    for (i = 0; i < n; ++i)
	printf(" %d", arr[i]);
    printf("\n");

    return 0;
}

void sort_array(size_t n, int arr[n])
{
    int i, j, a;

    for (i = 0; i < n; ++i) {
	for (j = i + 1; j < n; ++j) {
	    if (arr[i] > arr[j] && arr[i] % 2 != 0 && arr[j] % 2 != 0) {
		a = arr[i];
		arr[i] = arr[j];
		arr[j] = a;
	    }
	}
    }
}
