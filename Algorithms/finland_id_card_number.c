// Program to verify a Finnish ID number (HETU)

#include <stdio.h>
#define MAX 10 // number of digits including the check-digit

int i;
char idNumber[MAX + 1]; // Adding an element for the null byte

char validChars[31] = {'0', '1', '2', '3', '4', '5', '6', '7', '8',
                        '9', 'A', 'B', 'C', 'D', 'E', 'F', 'H', 'J',
                        'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'U',  
                        'V', 'W', 'X', 'Y'};

// Pass user input to array
void
getIdNumber(void)
{
        printf("Enter ID number (w/o '-' or '+', e.g. 120464126J): ");
        fgets(idNumber, MAX + 1, stdin);

        for (i = 0; i < 31; ++i)
                if (idNumber[MAX - 1] == validChars[i])
                        idNumber[MAX - 1] = i;

        for (i = 0; i < MAX - 1; ++i)
                idNumber[i] -= '0';
}

// Verify
void
verify(void)
{
        int initNumber = idNumber[0] * 10;

        for (i = 1; i < MAX - 1; ++i) {
                initNumber += idNumber[i];
                if (i < MAX - 2)
                        initNumber *= 10;
        }
        
        if (initNumber % 31 == idNumber[MAX - 1])
                printf("==> ID number is valid.");
        else
                printf("==> ID number is not valid.");

        printf("\n");
}        

int
main(void)
{
        void getIdNumber(void), verify(void);

        printf("-{*********************************}-\n");
        printf("  Verify a Finnish ID number (HETU)\n");
        printf("-{*********************************}-\n");

        getIdNumber();
        verify();

        return 0;
}