// Program to verify an Israeli ID card number

#include <stdio.h>

int i;
char id_number[10];

// Pass user input to array
void
get_id_number(void)
{
  printf("Enter ID number: ");
  fgets(id_number, 10, stdin);

  for (i = 0; i < 9; ++i)
    id_number[i] = id_number[i]-'0';
}

// Multiply every odd element by 2 
void
multiply_by_2(void)
{
  for (i = 1; i < 9; i += 2)
    id_number[i] *= 2; 
}

// Sum digits
void
sum_digits(void)
{
  int first_digit, second_digit;

  for (i = 1; i < 9; i += 2)
    if (id_number[i] > 9)
      {
	second_digit = id_number[i] % 10;
	first_digit = id_number[i] / 10;
	id_number[i] = second_digit + first_digit;
      }
}

// Verify
void
verify(void)
{
  int sum = 0;

  for (i = 0; i < 9; ++i)
    sum += id_number[i];
        
  if (sum % 10 == 0)
    printf("=>> ID number is valid.");
  else
    printf("=>> ID number is not valid.");

  printf("\n");
}        

int
main(void)
{
  void get_id_number(void), multiply_by_2(void), sum_digits(void), verify(void);

  get_id_number();
  multiply_by_2();
  sum_digits();
  verify();

  return 0;
}

